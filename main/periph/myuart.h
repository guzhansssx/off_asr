/*
 * @Author: your name
 * @Date: 2021-03-24 08:39:23
 * @LastEditTime: 2021-06-04 00:14:36
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \esp-adf\examples\myapp\off_asr\main\periph\myuart.h
 */
#ifndef _MYUART_H
#define _MYUART_H

int uart_init();
void rx_task();

#endif